#!bin/bash

SPRING_PROFILES_ACTIVE=prod REPLICA=2 envsubst < k8s.yml | kubectl apply --record=true -f -
kubectl rollout status deployments/${CI_PROJECT_NAME}-deployment -n=${K8S_NAMESPACE}